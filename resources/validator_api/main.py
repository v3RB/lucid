from flask import Flask, request, session, jsonify
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
import os, re

app = Flask(__name__)
dbpath = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(dbpath, 'validator.db')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

"""
DB has two tables Log,Users
"""


class Log(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user_id = db.Column(db.Integer, unique=False, nullable=False)
    time = db.Column(db.DateTime(), nullable=False, default=datetime.utcnow)

    def __repr__(self):
        return '<Token %r>' % self.token


class Users(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(40), unique=True)
    password = db.Column(db.String(40), nullable=False)

    def __repr__(self):
        return "{'user':%r,'pass':%r}" % (self.username, self.password)


def checkpass(user, passw):
    q = Users.query.filter_by(username=user, password=passw).first()
    if q is not None:
        return True
    return False
    

@app.route('/auth', methods=['POST','GET'])
def validator():
    db.create_all()
    try:
      username = request.headers.get('username',None) or ''
      password = request.headers.get('password',None) or ''
      if re.match('^[A-Z]*[a-z]*[0-9]*$', username):
        user = username
      else:
        user = ''
      if re.match('^[\w\d]+$', password):
        passw = password
      else:
        passw = ''
      if checkpass(user, passw):
        return jsonify({"message": "succesfully logged"}),200
      return jsonify({"message": "failed to authenticate"}),401
    except Exception:
      return jsonify({"message": "failed to authenticate/exception"}),401


if __name__ == '__main__':
    app.run(host="127.0.0.1",port=19090)
