#!/bin/sh

#move to script directory so all relative paths work
cd "$(dirname "$0")"

#includes
. ./config.sh 

sqlite3 /apps/validator/validator.db <<EOF
CREATE TABLE log (
	id INTEGER NOT NULL, 
	user_id INTEGER NOT NULL, 
	time DATETIME NOT NULL, 
	PRIMARY KEY (id)
);
CREATE TABLE users (
	id INTEGER NOT NULL, 
	username VARCHAR(40), 
	password VARCHAR(40) NOT NULL, 
	PRIMARY KEY (id), 
	UNIQUE (username)
);
EOF

sqlite3 /apps/validator/validator.db "INSERT INTO users(username,password) VALUES(\"$db_user\",\"$db_password\")"
